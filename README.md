# Repository for Nextflow Workflows used as part of the CGPS GHRU
The NIHR Global Health Research Unit: Genomic Surveillance of Antimicrobial Resistance aims to build capacity and standard procedures for processing pathogen genome data from sample through sequencing to genotypic AMR prediction.

This repository hosts code for [Nextflow](https://www.nextflow.io/) workflows that are in development as part of this project. For each one there is a matched docker file so that the pipelines are 'plug and play'.

## Installation
To clone all the pipelines
```
git clone --recurse-submodules https://gitlab.com/cgps/ghru/pipelines/pipelines.git
```
To use a single pipeline clone using the pipeline specific URLs in the Pipelines section below

## Running the workflows
In order to run the workflow you will need Nextflow to run the workflow and Docker to provide the dependencies (ssee Infrastructure section below).
The workflow is defined with a workflow file. Download the Nextflow workflow file ending in `.nf` and the `lib` directory containing the helper functions in `Helper.groovy`. This file and directory should be placed in the same directory when issuing the nextflow run command.

To run the pipeline you can check the required arguments by typing `nextflow run <WORKFLOW NAME>.nf --help`.
The generalised format is:

```
nextflow run <WORKFLOW NAME>.nf [options] -resume -with-docker bioinformant/ghru-<WORKFLOW NAME>:<VERSION> 
```
The matched docker image should be 'pulled' using the `docker pull` command. The docker version number should match the version of the workflow which can be determined by typing `nextflow run <WORKFLOW NAME>.nf --version`

For example the current version of the SPAdes assembly workflow is 1.2 so to run this the command would be

```
nextflow run assembly.nf [options] -resume -with-docker bioinformant/ghru-assembly:1.2
```

## Pipelines (work in progress)
The current pipelines are as follows. A more detailed description of each workflow can be found in the README within each workflow directory.
 - **[abricate](https://gitlab.com/cgps/ghru/pipelines/abricate)** Workflow to run Torsten Seemann's [abricate](https://github.com/tseemann/abricate) software to predict AMR genotypes from fasta sequences such as contigs or complete genomes
 - **[ariba](https://gitlab.com/cgps/ghru/pipelines/ariba)** Workflow to run Sanger pathogens [ariba](https://github.com/sanger-pathogens/ariba) software authored by Martin Hunt to predict AMR genotypes from fastq files
 - **[assembly](https://gitlab.com/cgps/ghru/pipelines/assembly)** Workflow to perform *de novo* assembly of short reads from bacterial species using [SPAdes](http://cab.spbu.ru/software/spades/) and fine tuned using the approaches recommended in [shovill](https://github.com/tseemann/shovill) by Torsten Seemann
 - **[phenix](https://gitlab.com/cgps/ghru/pipelines/phenix)** Workflow to perform SNP calling from short reads which are located locally on disc or fetched from the short read archive based on accession numbers. The workflow uses the [PHEnix](https://github.com/phe-bioinformatics/PHEnix) SNP calling pipeline from Public Health England

## Infrastructure for running the pipelines
 To run these pipelines a computing infrastructure with nextflow and docker installed. As a simple way of getting this running, if you have access to a ubuntu 18.04 box install anisble as follows:

 ```
 sudo apt -y update && sudo apt-get -y upgrade && sudo apt install -y  software-properties-common && sudo apt install -y ansible
 ```

 Then download the ansible playbook for the pipeline and run the playbook. E.g for the assembly pipeline
 
 ```
 wget https://gitlab.com/cgps/ghru/pipelines/assembly/raw/master/docker_and_nextflow.yml
 anisble-playbook docker_and_nextflow.yml
```
This will
  - Install Nextflow
  - Install Docker
  - Pull the docker image specific to the pipeline
  - Create a user called biouser with an expired password

Once you have run this playbook logout and logback in again as biouser e.g `ssh biouser@IP_ADDRESS`. This will prompt you to enter the current login details (`bio101`) and enter a new password.
As biouser you have sudo and docker privileges. You can upload data to this user's directory and run nextflow as that user.

I have also published a [blog post](https://antunderwood.gitlab.io/bioinformant-blog/posts/running_nextflow_on_aws_batch/) on how you can run this on [AWS Batch](https://aws.amazon.com/batch/)

