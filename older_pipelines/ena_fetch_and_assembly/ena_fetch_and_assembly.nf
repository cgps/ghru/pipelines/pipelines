#!/usr/bin/env nextflow
/*

========================================================================================
                          Assembly Pipeline
========================================================================================
 Assembly pipeline based on Shovill (Thanks Torsten Seemann)
 #### Authors
 Anthony Underwood @bioinformant <au3@sanger.ac.uk>
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.0'
//  print help if required
def helpMessage() {
    log.info"""
    =========================================
     Assembly Pipeline v${version}
    =========================================
    This workflow will download fastqs based on SRR numbers from GenBank
    Mandatory arguments:
      --input_dir      Path to input dir "must be surrounded by quotes"
      --output_dir     Path to output dir "must be surrounded by quotes"
      --fastq_pattern  The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
      --adapter_sequences  The path to a fasta file containing adapter sequences to trim from reads
   """.stripIndent()
}

params.help = false
// Show help message
if (params.help){
    helpMessage()
    exit 0
}

/***************** Setup inputs and channels ************************/
// Defaults for configurable variables
params.accession_number_file = false
params.output_dir = false
params.fastq_pattern = false
params.adapter_sequences = false
params.depth_cutoff = false

// set up accessions
accession_number_file = Helper.check_mandatory_parameter(params, 'accession_number_file') - ~/\/$/
// set up output directory
output_dir = Helper.check_mandatory_parameter(params, 'output_dir') - ~/\/$/
//  check a pattern has been specified
fastq_pattern = Helper.check_mandatory_parameter(params, 'fastq_pattern')
//  check an adapter_file has been specified
adapter_file = Helper.check_mandatory_parameter(params, 'adapter_file')
// assign depth_cutoff from params
depth_cutoff = params.depth_cutoff

log.info "======================================================================"
log.info "                  Fetch from ENA and Assembly pipeline"
log.info "======================================================================"
log.info "Running version      : ${version}"
log.info "Accession Number File: ${accession_number_file}"
log.info "Adapter file         : ${adapter_file}"
log.info "Depth cutoff         : ${depth_cutoff}"
log.info "======================================================================"
log.info "Outputs written to  '${output_dir}'"
log.info "======================================================================"
log.info ""


Channel
     .fromPath(accession_number_file)
     .splitText()
     .map{ x -> x.trim()}
     .set { accession_numbers }

process fetch_from_ena {
  tag { accession_number }
  
  publishDir "${output_dir}/fastqs",
    mode: 'copy',
     saveAs: { file -> file.split('\\/')[-1] }

  input:
  val accession_number from accession_numbers

  output:
  set accession_number, file("${accession_number}/*.fastq.gz") into raw_fastqs

  """
  enaDataGet -f fastq ${accession_number}
  """
}

// duplicate raw fastq channel for trimming and qc
raw_fastqs.into {raw_fastqs_for_qc; raw_fastqs_for_trimming}

// Pre-Trimming QC
process qc_pre_trimming {
  tag { accession_number }
  
  publishDir "${output_dir}/qc_pre_trimming",
    mode: 'copy',
    pattern: "*.html"

  input:
  set accession_number, file(file_pair)  from raw_fastqs_for_qc

  output:
  file('*.html')

  """
  fastqc ${file_pair[0]} ${file_pair[1]}
  """
}

// Trimming
process trimming {
  tag { accession_number }
  
  input:
  set accession_number, file(file_pair)  from raw_fastqs_for_trimming

  output:
  set accession_number, file('trimmed_fastqs/*.fastq.gz') into trimmed_fastqs_for_qc, trimmed_fastqs_for_correction, trimmed_fastqs_for_genome_size_estimation, trimmed_fastqs_for_read_counting

  """
  mkdir trimmed_fastqs
  trimmomatic PE ${file_pair[0]} ${file_pair[1]} trimmed_fastqs/${file_pair[0]} /dev/null trimmed_fastqs/${file_pair[1]} /dev/null ILLUMINACLIP:${adapter_file}:2:30:10 SLIDINGWINDOW:4:20 LEADING:25 TRAILING:25 MINLEN:30  
  """
}

// Post-Trimming QC
process qc_post_trimming {
  tag { accession_number }

  publishDir "${output_dir}/qc_post_trimming",
    mode: 'copy',
    pattern: "*.html"
  
  input:
  set accession_number, file(file_pair)  from trimmed_fastqs_for_qc

  output:
  file('*.html')

  """
  fastqc ${file_pair[0]} ${file_pair[1]}
  """
}


// Genome Size Estimation
process genome_size_estimation {
  tag { accession_number }
  
  input:
  set accession_number, file(file_pair)  from trimmed_fastqs_for_genome_size_estimation

  output:
  set accession_number, file('mash_stats.out') into mash_output

  """
  mash sketch -o /tmp/sketch_${accession_number}  -k 32 -m 3 -r ${file_pair[0]}  2> mash_stats.out
  """
}

def find_genome_size(accession_number, mash_output) {
  m = mash_output =~ /Estimated genome size: (.+)/
  genome_size = Float.parseFloat(m[0][1]).toInteger()
  return [accession_number, genome_size]
}

// channel to output genome size from mash output
mash_output.map { accession_number, file -> find_genome_size(accession_number, file.text) }.into{genome_size_estimation_for_read_correction; genome_size_estimation_for_downsampling}

trimmed_fastqs_and_genome_size = trimmed_fastqs_for_correction.join(genome_size_estimation_for_read_correction).map{ tuple -> [tuple[0], tuple[1], tuple[2]]}
// trimmed_fastqs_and_genome_size.subscribe{println it}

// Read Corection
process read_correction {
  tag { accession_number }
  
  publishDir "${output_dir}",
    mode: 'copy',
    pattern: "corrected_fastqs/*.fastq.gz"

  input:
  set accession_number, file(file_pair), genome_size from trimmed_fastqs_and_genome_size

  output:
  set accession_number, file('lighter.out') into read_correction_output
  set accession_number, file('corrected_fastqs/*.fastq.gz') into corrected_fastqs

  """
  lighter -od corrected_fastqs -r  ${file_pair[0]} -r  ${file_pair[1]} -K 32 ${genome_size}  -maxcor 1 2> lighter.out
  for file in corrected_fastqs/*.cor.fq.gz
  do
      new_file=\${file%.cor.fq.gz}.fastq.gz
      mv \${file} \${new_file}
  done
  """
}

// def find_average_depth(accession_number, lighter_output){
//   m = lighter_output =~  /.+Average coverage is ([0-9]+\.[0-9]+)\s.+/
//   average_depth = Float.parseFloat(m[0][1])
//   return [accession_number, average_depth]
// }


// Estimate total number of reads
process count_number_of_reads {
  tag { accession_number }
  
  input:
  set accession_number, file(file_pair) from trimmed_fastqs_for_read_counting

  output:
  set accession_number, file('seqtk_fqchk.out') into seqtk_fqchk_output

  """
  seqtk fqchk -q 25 ${file_pair[0]} > seqtk_fqchk.out
  """
}

def find_total_number_of_reads(accession_number, seqtk_fqchk_ouput){
  m = seqtk_fqchk_ouput =~ /ALL\s+(\d+)\s/
  total_reads = m[0][1].toInteger() * 2 // the *2 is an estimate since number of reads >q25 in R2 may not be the same
  return [accession_number, total_reads]
}
read_counts = seqtk_fqchk_output.map { accession_number, file -> find_total_number_of_reads(accession_number, file.text) }


corrected_fastqs_and_genome_size_and_read_count = corrected_fastqs.join(genome_size_estimation_for_downsampling).join(read_counts).map{ tuple -> [tuple[0], tuple[1], tuple[2], tuple[3]]}

// merge reads and potentially downsample
process merge_reads{
  tag { accession_number }

  publishDir "${output_dir}",
    mode: 'copy',
    pattern: "merged_fastqs/*.fastq.gz"
  
  input:
  set accession_number, file(file_pair), genome_size, read_count from corrected_fastqs_and_genome_size_and_read_count

  output:
  set accession_number, file('merged_fastqs/*.fastq.gz') into merged_fastqs

  script:
  
  if (depth_cutoff  && read_count/genome_size > depth_cutoff.toInteger()){
    downsampling_factor = depth_cutoff.toInteger()/(read_count/genome_size)
    """
    mkdir downsampled_fastqs
    seqtk sample  ${file_pair[0]} ${downsampling_factor} | gzip > downsampled_fastqs/${file_pair[0]}
    seqtk sample  ${file_pair[1]} ${downsampling_factor} | gzip > downsampled_fastqs/${file_pair[1]}
    flash -m 20 -M 100 -d merged_fastqs -o ${accession_number} -z downsampled_fastqs/${file_pair[0]} downsampled_fastqs/${file_pair[1]} 
    """
  } else {
    """
    flash -m 20 -M 100 -d merged_fastqs -o ${accession_number} -z ${file_pair[0]} ${file_pair[1]} 
    """
  }

}

// assemble reads
process spades_assembly {
  tag { accession_number }

  publishDir "${output_dir}/assembly",
  mode: 'copy'

  input:
  set accession_number, file(file_triplet) from merged_fastqs

  output:
  set accession_number, file("${accession_number}_contigs.fasta") into contigs_for_single_analysis, contigs_for_combined_analysis

  """
  spades.py --pe1-1 ${file_triplet[1]} --pe1-2 ${file_triplet[2]} --pe1-m ${file_triplet[0]} --only-assembler  -o . --tmp-dir /tmp/${accession_number}_assembly -k 21,33,43,53,63,75 --memory 4 --threads 1
  ln -s contigs.fasta ${accession_number}_contigs.fasta
  """

}

// assess assembly with Quast
process quast {
  tag { accession_number }
    
  publishDir "${output_dir}",
    mode: 'copy',
    pattern: "report.tsv",
    saveAs: { file -> "${accession_number}_quast_${file}"}

  input:
  set accession_number, file(contig_file) from contigs_for_single_analysis

  output:
  set accession_number, file('report.tsv')

  """
  quast.py ${contig_file} -o .
  """
}

// assess assembly with Quast but in a single file
process quast_summary {
  tag { 'quast_summary' }
  
  publishDir "${output_dir}",
    mode: 'copy',
    pattern: "report.tsv",
    saveAs: { file -> "combined_quast_report.tsv"}

  input:
  file(contig_files) from contigs_for_combined_analysis.collect { it[1] }

  output:
  file('report.tsv')

  """
  quast.py ${contig_files} -o .
  """
}

workflow.onComplete {
  Helper.complete_message(params, workflow, version)
}

workflow.onError {
  Helper.error_message(workflow)
}
